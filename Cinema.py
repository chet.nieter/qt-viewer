#!/usr/bin/python

#  @description Cinema viewer application. Supports loading multiple databases
#  simultaneously. Either single or a multi-database directories can be loaded
#  directly from the file menu or by passing the top info.json as an argument.
#
#  - Single database
#  Pass the database's info.json path as an argument.
#
#  - Multiple databases
#  Pass the top-directory's info.json path as an argument. The top-directoy
#  info.json is expected to have the following format:
#
#  {
#     "metadata": {
#         "type": "workbench"
#     },
#     "runs": [
#         {
#         "title": "image_0",
#         "description": "image_0",
#         "path": "image_0"
#         },
#         {
#         "title": "image_1",
#         "description": "image_1",
#         "path": "image_1"
#         }
#     ]
# }
#
# Where "path" is the name of the directory containing the info.json of a
# particular database.

import os
if os.path.exists("./widgets/uiFiles"):
    # compile ui files as long as the dir exists
    import pysideuic as uic
    uic.compileUiDir("./widgets/uiFiles")

import sys                              # noqa: E402
from PySide.QtGui import QApplication   # noqa: E402
from MultiViewerMain import ViewerMain  # noqa: E402
from common import DatabaseLoader       # noqa: E402

# ---------------------------------------------------------------------
# create the qt app
a = QApplication(sys.argv)
v = ViewerMain()

# parse arguments if any
argv = sys.argv
if len(argv) > 1:
    databases, links = DatabaseLoader.loadAll(sys.argv[1])
    v.setStores(databases, links)

# ------------------- start profiling -------------------------------
# import cProfile, pstats, StringIO
# pr = cProfile.Profile()
# pr.enable()
# -------------------------------------------------------------------

v.show()
qAppRet = a.exec_()

# --------------------- finish profiling ----------------------------
# pr.disable()
# s = StringIO.StringIO()
# sortby = 'cumulative'
# ps = pstats.Stats(pr, stream=s).sort_stats(sortby)
# ps.print_stats(0.1) # print 10% of calls
# print s.getvalue()
# -------------------------------------------------------------------

sys.exit(qAppRet)

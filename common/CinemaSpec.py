class CinemaSpec():
    ''' Helper class used to manage Spec versions in applications.
    '''
    A = 0
    B = 1
    C = 2

    def resolveStoreVersion(self, store):
        associations = store.parameter_associations
        parameters = store.parameter_list

        if len(associations) == 0:
            return CinemaSpec.A
        elif ("vis" in parameters) and \
             (parameters["vis"]["role"] == "layer"):
            return CinemaSpec.B
        else:
            raise AttributeError("Unknown Cinema database format!")

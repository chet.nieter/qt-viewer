import math
from PySide.QtCore import Signal
from RenderViewMouseInteractor import RenderViewMouseInteractor


class PhiThetaInteractor(RenderViewMouseInteractor):
    """ mouse interaction for phi-theta type stores """

    phiChanged = Signal(int)
    thetaChanged = Signal(int)

    def __init__(self):
        super(PhiThetaInteractor, self).__init__()

        # xPhiRatio
        # How many pixels must be dragged in x per degree change in phi.
        self._phi = 0
        self._theta = 0
        self.xPhiRatio = 1  # ?

        # yThetaRatio
        # How many pixels must be dragged in y per degree change in theta.
        self.yThetaRatio = 1  # ?

        self._stepPhi = 30
        self._stepTheta = 30

    def initializePhiValues(self, param):
        self.setPhiValues(param["values"])
        self.setPhi(param["default"])

    def initializeThetaValues(self, param):
        self.setThetaValues(param["values"])
        self.setTheta(param["default"])

    def setPhiValues(self, phiValues):
        self._phiValues = phiValues

        # Warning -
        # this assumes an even angle spacing through the phiValues array.
        if len(phiValues) > 1:
            self._stepPhi = phiValues[1] - phiValues[0]
        else:
            self._stepPhi = 0

    def setThetaValues(self, thetaValues):
        self._thetaValues = thetaValues

        # Warning -
        # this assumes an even angle spacing through the thetaValues array.
        if len(thetaValues) > 1:
            self._stepTheta = thetaValues[1] - thetaValues[0]
        else:
            self._stepTheta = 0

    def setPhi(self, phi):
        self._phi = phi

    def setTheta(self, theta):
        self._theta = theta

    def getPhi(self):
        return self._phi

    def getTheta(self):
        return self._theta

    def updateCamera(self):
        self.phiChanged.emit(self._phi)
        self.thetaChanged.emit(self._theta)

    def mouseMove(self, x, y, dx, dy):
        dphi = dx / self.xPhiRatio
        dtheta = dy / self.yThetaRatio
        phi_sign = 1 if dphi > 0 else -1
        theta_sign = 1 if dtheta > 0 else -1

        # If the phi angles are not evenly spaced, this logic won't work.
        # Should look at angle above and below this one to make the increment
        # decision.
        if (math.fabs(dphi) > self._stepPhi):
            self._phi = self._incrementAngle(self._phi, phi_sign,
                                             self._phiValues)
            self._xy = (x, y)

        # The same comment for the phi update holds for the theta update.
        if (math.fabs(dtheta) > self._stepTheta):
            self._theta = self._incrementAngle(self._theta, theta_sign,
                                               self._thetaValues)
            self._xy = (x, y)

    def _incrementAngle(self, angle, sign, angles):
        # Increment angle to be either the next or previous angle in the angle
        # list
        # Find index of angle in array of angles
        index = angles.index(angle)
        index = index + sign * 1
        if (index < 0):
            index = len(angles)-1
        if (index >= len(angles)):
            index = 0
        return angles[index]

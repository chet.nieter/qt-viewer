from cinema_python.images.lookup_tables import LookupTable


class PropertyItem:
    ''' Container for specific rendering options (geometry color, color lut,
    etc.) of a parameter's property
    (e.g. [Parameter]Calculator1 -> [Property]coord_x_2).
    This is intended to become the PropertyModel table item. '''
    def __init__(self, colorLut=LookupTable(), geometryColor=[255, 255, 255]):
        self.__colorLut = colorLut
        self.__geometryColor = geometryColor
        self.__valueRange = tuple([None, None])
        # Convention: [0]-> displayed param name; [1]-> displayed vis
        # self.__displayData = displayData

    def colorLut(self):
        return self.__colorLut

    def geometryColor(self):
        return self.__geometryColor

    def valueRange(self):
        return self.__valueRange

    def setColorLut(self, lut):
        self.__colorLut = lut

    def setGeometryColor(self, color):
        self.__geometryColor = color

    def setValueRange(self, valRange):
        self.__valueRange = valRange

#    def data(self, column):
#    def setData(self, column, value):
#    def columnCount(self):
#    def row(self):

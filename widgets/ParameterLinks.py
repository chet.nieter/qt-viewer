class ParameterLinks():
    '''
    Manages Links between parameters in independent views
    When viewer opens a multiview database,
    '''
    def __init__(self, links_list):
        self.__links_list = links_list
        self.__controllers = None

    def SetControllers(self, controllers):
        self.__controllers = controllers

    def Update(self, originating_controller, originating_store, totalQuery):
        if not self.__links_list:
            return
        for k, v in totalQuery.iteritems():
            if k in self.__links_list:
                # todo: so far only know how to update a few things, make sure
                # we can do all of them consider making ControllerWidget more
                # structured in what it makes to do that then the update block
                # below would be one single call

                # todo: provide a way to enable/disable specific links in cases
                # where user want them to vary independently

                if k == 'pose':
                    val = totalQuery[k]
                else:
                    # todo, how about params that take multiple?
                    val = list(iter(totalQuery[k]))[0]
                index = originating_store.get_parameter(k)['values'].index(val)

                for other in self.__controllers:
                    if other != originating_controller:
                        if k == 'time':
                            otherstime = \
                                other._ControllersWidget__ui.sliderTime
                            otherstime._onValueChanged(index)
                        elif k == 'phi':
                            otherstime = other._ControllersWidget__ui.sliderPhi
                            otherstime._onValueChanged(index)
                        elif k == 'theta':
                            otherstime = \
                                other._ControllersWidget__ui.sliderTheta
                            otherstime._onValueChanged(index)
                        elif k == 'pose':
                            # todo: get hold of the Interactor and set the
                            # current pose
                            pass
                        else:
                            for slider in \
                                    other._ControllersWidget__sliderParams:
                                if k in slider.getQuery():
                                    slider._onValueChanged(index)

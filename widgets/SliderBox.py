from PySide import QtCore
from PySide.QtCore import Signal, QTimer
from PySide.QtGui import QGroupBox, QStyle
from widgets.uiFiles import ui_SliderBox as ui


class SliderBox(QGroupBox):
    # Qt signals in PySide style
    # intended to trigger rendering events
    queryChanged = Signal()
    # intended to synchronize with another controller (e.g. interactor)
    parameterChanged = Signal(int)

    def __init__(self, parent):
        super(SliderBox, self).__init__(parent)
        self._ui = ui.Ui_SliderBox()
        self._values = None
        self._parameterName = None
        self._query = None
        self._playTimer = QTimer(self)

        self._ui.setupUi(self)
        self._playTimer.setInterval(5)  # arbitrarily chosen small number
        self.hide()

        # default theme icons are specified directly in the ui_SliderBox.ui,
        # for details see:
        # http://5in4.de/blog/2012/02/07/using-standard-icons-in-qt-designer-and-qt-creator/ # noqa
        # http://standards.freedesktop.org/icon-naming-spec/icon-naming-spec-latest.html # noqa

        # this hack is necessary given that setting default theme icons in *.ui
        # does not currently work
        self._ui._pbPlay.setIcon(
            self.style().standardIcon(QStyle.SP_MediaPlay))
        self._ui._pbSeekBackward.setIcon(
            self.style().standardIcon(QStyle.SP_MediaSeekBackward))
        self._ui._pbSkipBackward.setIcon(
            self.style().standardIcon(QStyle.SP_MediaSkipBackward))
        self._ui._pbSeekForward.setIcon(
            self.style().standardIcon(QStyle.SP_MediaSeekForward))
        self._ui._pbSkipForward.setIcon(
            self.style().standardIcon(QStyle.SP_MediaSkipForward))

        # internal connection to update label
        self._ui._sliderValue.valueChanged.connect(self._onValueChanged)
        self._ui._pbSeekBackward.clicked.connect(self._onSeekBackward)
        self._ui._pbSkipBackward.clicked.connect(self._onSkipBackward)
        self._ui._pbSeekForward.clicked.connect(self._onSeekForward)
        self._ui._pbSkipForward.clicked.connect(self._onSkipForward)
        self._ui._pbPlay.toggled.connect(self._onPlayToggled)
        self._playTimer.timeout.connect(self._onPlayTimerUpdate)

    def _onValueChanged(self, index):
        value = self._values[index]
        if self._ui._laValue.text() == '{0}'.format(value):
            return
        self._ui._laValue.setText('{0}'.format(value))
        self._generateQuery(value)
        self.queryChanged.emit()
        self.parameterChanged.emit(value)

    def _onSeekBackward(self):
        sl = self._ui._sliderValue
        minValue = sl.minimum()
        sl.setValue(minValue if sl.value() == minValue else sl.value() - 1)

    def _onSkipBackward(self):
        sl = self._ui._sliderValue
        sl.setValue(sl.minimum())

    def _onSeekForward(self):
        sl = self._ui._sliderValue
        maxValue = sl.maximum()
        sl.setValue(maxValue if sl.value() == maxValue else sl.value() + 1)

    def _onSkipForward(self):
        sl = self._ui._sliderValue
        sl.setValue(sl.maximum())

    @QtCore.Slot(bool)
    def _onPlayToggled(self, status):
        if status:
            # reset if starting from maximum
            sl = self._ui._sliderValue
            if sl.value() == sl.maximum():
                sl.setValue(0)

            self._playTimer.start()

        else:
            self._playTimer.stop()

    @QtCore.Slot()
    def _onPlayTimerUpdate(self):
        sl = self._ui._sliderValue
        if sl.value() < sl.maximum():
            sl.setValue(sl.value() + 1)
        else:
            self._ui._pbPlay.setChecked(False)  # triggers _onPlayToggled

    def _generateQuery(self, value):
        self._query = {self._parameterName: set([value])}

    def onParameterChanged(self, value):
        if value in self._values:
            index = self._values.index(value)
            # valueChanged is only emitted if the value is different (prevents
            # infinite loop in cyclic connections)
            self._ui._sliderValue.setValue(index)

    def configureSlider(self, parameters, name):
        self.show()
        self._parameterName = name
        parameter = parameters[name]
        self._values = parameter['values']
        self._ui._sliderValue.setRange(0, len(self._values) - 1)
        self._ui._sliderValue.setPageStep(1)
        defaultValue = (parameter['default'] if ('default' in parameter)
                        else self._values[0])
        index = self._values.index(defaultValue)
        # hack to update label on init (only emits signal if
        # value is different)
        self._ui._sliderValue.setValue(index + 1)
        self._ui._sliderValue.setValue(index)
        self._generateQuery(defaultValue)

    def getQuery(self):
        return self._query

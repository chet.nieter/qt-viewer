from PySide import QtCore
from PySide.QtCore import Qt, QModelIndex
from PySide.QtGui import QColorDialog, QWidget
from PySide.QtGui import QItemSelectionModel as QISM

from widgets import SliderBox
from uiFiles import ui_ControllersWidget as ui
from common.CinemaSpec import CinemaSpec
from PipelineModel import PipelineModel


class ControllersWidget(QWidget):
    ''' Contains and manages a set of controller widgets used to navigate
    parameters in a Cinema store.

    It holds a reference to a CameraWidget (set during intialization) which is
    updated when the parameter selection changes. Widgets for color
    customization are also contained here.'''
    def __init__(self, parent):
        super(ControllersWidget, self).__init__(parent)
        self.__ui = ui.Ui_ControllersWidget()
        self.__plModel = PipelineModel()
        self.__currentQuery = None
        self.__sliderParams = []
        self.__cameraWidget = None
        self.__colorPicker = QColorDialog(self)
        self.__store = None
        self.__poseNum = 0
        self.__plinks = None

        # create ui
        self.__ui.setupUi(self)

        # setup model and view
        self.__ui.tviewLayers.setModel(self.__plModel)
        tvHeader = self.__ui.tviewLayers.header()
        tvHeader.swapSections(1, 0)
        tvHeader.resizeSection(1, 70)

        self.__ui.pbSetGeometryColor.clicked.connect(self.__onSetGeometryColor)
        self.__plModel.valueRangeChanged.connect(
                self.__ui.wColorBar.setMinMaxValues)

    @QtCore.Slot(tuple)
    def __onColorLutChanged(self, colorLutTuple):
        ''' Receives a tuple of LookupTable.  The only reason for using a tuple
        is that PySide (Qt) cannot include user-defined python classes in a
        Signal's signature.'''
        # This check is necessary given that _wColorBar.colorLutChanged()
        # causes a call to this slot even before being connected.
        if self.__currentQuery is None:
            return

        sm = self.__ui.tviewLayers.selectionModel()
        self.__plModel.setColorLut(sm.currentIndex(), colorLutTuple[0])
        self.__cameraWidget.update(self.__currentQuery,
                                   self.__plModel.getColorDefinitions())

    @QtCore.Slot(bool)
    def __enablePropertyWidgets(self, enable):
        self.__ui.gbColorLut.setVisible(enable)
        self.__ui.gbGeometryColor.setVisible(enable)

    @QtCore.Slot()
    def __onSetGeometryColor(self):
        if self.__colorPicker.exec_() == self.__colorPicker.Accepted:
            sm = self.__ui.tviewLayers.selectionModel()
            color = self.__colorPicker.selectedColor().getRgb()
            self.__plModel.setGeometryColor(sm.currentIndex(), color)
            self.__cameraWidget.update(self.__currentQuery,
                                       self.__plModel.getColorDefinitions())

    @QtCore.Slot()
    def __onQueryChanged(self):
        ''' Computes the global ControllersWidget query which will be passed to
        the rendering instance (CameraWidget).'''
        totalQuery = dict()

        # Add base queries
        queryPhi = self.__ui.sliderPhi.getQuery()
        if queryPhi:
            totalQuery.update(queryPhi)

        queryTheta = self.__ui.sliderTheta.getQuery()
        if queryTheta:
            totalQuery.update(queryTheta)

        queryTime = self.__ui.sliderTime.getQuery()
        if queryTime:
            totalQuery.update(queryTime)

        # Add parameter model queries
        queryPlModel = self.__plModel.getQuery()
        if queryPlModel:
            totalQuery.update(queryPlModel)

        self.__addOtherSliderQueries(totalQuery)

        if 'pose' in self.__store.parameter_list.keys():
            pose = self.__store.get_parameter('pose')['values'][self.__poseNum]
            totalQuery.update({'pose': pose})

        self.__currentQuery = totalQuery
        self.__cameraWidget.update(self.__currentQuery,
                                   self.__plModel.getColorDefinitions())

        self._updateLinkedParameters(totalQuery)

    def SetLinks(self, plinks):
        self.__plinks = plinks

    def _updateLinkedParameters(self, totalQuery):
        if self.__plinks is None:
            return
        self.__plinks.Update(self, self.__store, totalQuery)

    def __expandTreeView(self):
        root = self.__plModel.index(0, 0, QModelIndex())
        allIndices = self.__plModel.match(root, Qt.DisplayRole, "*", -1,
                                          Qt.MatchWildcard | Qt.MatchRecursive)
        for i in allIndices:
            self.__ui.tviewLayers.expand(i)

    def __setDefaultSelection(self):
        # default = root model index
        index = self.__plModel.index(0, 0, QModelIndex())
        self.__ui.tviewLayers.selectionModel().setCurrentIndex(
            index, QISM.SelectionFlag.Select)

    def __connectUiQueries(self):
        ''' Connects each controller's query to the handler that generates a
        the global ControllersWidget query.

        Warning: this method does not connect the signals from 'range-value'
        sliders other than time/camera. Those are connected on creation from
        within self.__createRangeParameters. '''
        self.__ui.tviewLayers.clicked.connect(self.__plModel.onViewClicked)
        self.__plModel.queryChanged.connect(self.__onQueryChanged)

        self.__ui.sliderTime.queryChanged.connect(self.__onQueryChanged)
        self.__ui.sliderPhi.queryChanged.connect(self.__onQueryChanged)
        self.__ui.sliderTheta.queryChanged.connect(self.__onQueryChanged)

        sm = self.__ui.tviewLayers.selectionModel()
        sm.currentChanged.connect(self.__plModel.onSelectedItemChanged)

    def __populateParameters(self, store, version, cameraWidget, defaultLut):
        self.__store = store
        storeParameters = store.parameter_list
        keys = sorted(storeParameters)

        # special range keys that are most common
        keys_special = []
        for special in ['time', 'phi', 'theta', 'pose']:
            if special in keys:
                keys.remove(special)
                keys_special.append(special)

        # setup special parameters if available
        if 'time' in keys_special:
            self.__ui.sliderTime.configureSlider(storeParameters, 'time')

        rwi = cameraWidget.mouseInteractor()

        if 'phi' in keys_special:
            self.__ui.sliderPhi.configureSlider(storeParameters, 'phi')
            rwi.initializePhiValues(storeParameters['phi'])
            rwi.phiChanged.connect(self.__ui.sliderPhi.onParameterChanged)
            self.__ui.sliderPhi.parameterChanged.connect(rwi.setPhi)

        if 'theta' in keys_special:
            self.__ui.sliderTheta.configureSlider(storeParameters, 'theta')
            rwi.initializeThetaValues(storeParameters['theta'])
            rwi.thetaChanged.connect(self.__ui.sliderTheta.onParameterChanged)
            self.__ui.sliderTheta.parameterChanged.connect(rwi.setTheta)

        if 'pose' in keys_special:
            rwi.newPose.connect(self._newPose)
            rwi.initializePoses(storeParameters['pose'])
            self.__poseNum = rwi.posenum

        if ('phi' in keys_special or 'theta' in keys_special or
                'pose' in keys_special):
            cameraWidget.disconnectInteractorSignals()
            cameraWidget.connectInteractorSignals()

        # create other sliders for range parameters
        remainingKeys = self.__createRangeParameters(storeParameters, keys)

        # parse the remaining keys (if any) and include them in the PlModel
        if len(remainingKeys) > 0:
            self.__plModel.setDefaultColorLut(defaultLut)
            self.__plModel.populate(store, remainingKeys, version)
            self.__expandTreeView()
        else:
            self.__ui.tviewLayers.hide()

    @QtCore.Slot(int)
    def _newPose(self, posenum):
        ''' This is method is specificallly used with the ArcBallInteractor.
        TODO: consider moving somewhere else. '''
        self.__poseNum = posenum
        pose = self.__store.get_parameter('pose')['values'][self.__poseNum]
        self.__currentQuery.update({'pose': pose})
        self.__onQueryChanged()

    def __createRangeParameters(self, storeParameters, keys):
        remainingParameterKeys = []
        for k in keys:
            if k == 'pose':
                continue

            if storeParameters[k]['type'] == 'range':
                s = SliderBox.SliderBox(self.__ui.wRangeParams)
                s.configureSlider(storeParameters, k)

                if ('label' in storeParameters[k]):
                    s.setTitle(storeParameters[k]['label'])
                else:
                    s.setTitle(storeParameters[k])

                s.queryChanged.connect(self.__onQueryChanged)
                self.__ui.wRangeParams.layout().addWidget(s)
                self.__sliderParams.append(s)

                if len(storeParameters[k]['values']) <= 1:
                    s.hide()
            else:
                remainingParameterKeys.append(k)

        if len(self.__sliderParams) == 0:
                self.__ui.saRangeParams.hide()

        return remainingParameterKeys

    def __connectControllerSignals(self):
        ''' Connects signals for interaction between different controllers
        (e.g. PipelineModel with ColorBarWidget, etc.).'''
        self.__plModel.itemVisibilityChanged.connect(
            self.__enablePropertyWidgets)
        self.__plModel.currentColorLutChanged.connect(
            self.__ui.wColorBar.setCurrentLut)
        self.__plModel.valueRangeChanged.connect(
            self.__ui.wColorBar.setMinMaxValues)
        self.__ui.wColorBar.colorLutChanged.connect(self.__onColorLutChanged)
        self.__ui.cbUseGeometryColor.stateChanged.connect(
            self.triggerDisplayUpdate)
        self.__ui.cbUseLighting.stateChanged.connect(self.triggerDisplayUpdate)

    def __addOtherSliderQueries(self, totalQuery):
        if len(self.__sliderParams) > 0:
            for slider in self.__sliderParams:
                q = slider.getQuery()
                if q:
                    totalQuery.update(q)

    @QtCore.Slot()
    def triggerDisplayUpdate(self):
        self.__cameraWidget.update(self.__currentQuery,
                                   self.__plModel.getColorDefinitions())

    def setStore(self, store, spec, cameraWidget, cameraId):
        ''' Initialization. '''
        if spec == CinemaSpec.A:
            self.__ui.gbColorLut.hide()
            self.__ui.gbGeometryColor.hide()
        elif spec == CinemaSpec.B:
            self.__ui.wColorBar.setCurrentLut("ParaView")

        # Setup the CameraWidget
        self.__cameraWidget = cameraWidget
        self.__cameraWidget.setStore(store, spec)
        self.__cameraWidget.setId(cameraId)
        self.__ui.cbUseGeometryColor.stateChanged.connect(self.__cameraWidget.
                                                          enableGeometryColor)
        self.__ui.cbUseLighting.stateChanged.connect(
            self.__cameraWidget.enableLighting)

        # Setup the controllers
        self.__connectControllerSignals()
        defaultLut = self.__ui.wColorBar.getCurrentColorLut()
        self.__populateParameters(store, spec, cameraWidget, defaultLut)
        self.__connectUiQueries()
        self.__setDefaultSelection()

        # Initialize the query and display (default values)
        self.__onQueryChanged()
